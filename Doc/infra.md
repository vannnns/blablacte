# Global

Une application web asp.net core.  
Un batch qui tourne toutes les 4 heures pour récupérer les données de l'open data et les vérifier.

# Systemd

## Service

L'application web tourne sous un service systemd.

```systemd
cat /etc/systemd/system/blablactesweb.service
[Unit]
Description=Blablactes web server
After=network.target

[Service]
Type=simple
#Restart=always
#RestartSec=1
ExecStart=/srv/blablactes/web/startWebSrv.sh

[Install]
WantedBy=multi-user.target
```

Le script de démarrage du serveur web
```bash
dotnet Web.App.dll --opendatadirectory /srv/blablactes/data
```

## Timer

Le batch est lancé via timer systemd
```systemd
cat /etc/systemd/system/blablactesbatch.service
[Unit]
Description=Blablactes batch
After=network.target

[Service]
Type=simple
ExecStart=/srv/blablactes/batch/updateData.sh

[Install]
WantedBy=multi-user.target
```

```systemd
cat /etc/systemd/system/blablactesbatch.timer
[Unit]
Description=Download last french assembly open data version

[Timer]
OnBootSec=1min
OnUnitActiveSec=4h

[Install]
WantedBy=timers.target
```

```bash
cat updateData.sh
#!/bin/bash

urlScrutins="https://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.json.zip"
urlOrganesActeurs="https://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises_XV.json.zip"
srvdir="/srv/blablactes"
refdir="$srvdir/data"
tmpdir="/tmp/blablactes/"
datadir="/tmp/blablactes/data"
tmpScrutins="scrutins.zip"
tmpAO="ao.zip"
scrutinsZip="$tmpdir$tmpScrutins"
aoZip="$tmpdir$tmpAO"

mkdir -p $datadir

echo "Downloading scrutins..."
curl -o $scrutinsZip $urlScrutins
echo "done"

echo "Downloading organesActeurs..."
curl -o $aoZip $urlOrganesActeurs
echo "done"

echo "Unziping $scrutinsZip to $datadir/scrutin"
unzip -qo $scrutinsZip -d "$datadir/scrutin"

echo "Unziping $aoZip to $datadir"
unzip -qo $aoZip -d $datadir
echo "Unzipping OK"

echo "Check data consistency..."
dotnet $srvdir/batch/assemblydatavalidator/AssemblyDataValidator.dll --opendatadirectory $datadir

if [ $? -eq 0 ]
then
  echo "Data consistency OK."
else
  echo "ERROR : Data consistency KO"
  exit 1
fi

echo "Copying data to $refdir ..."
cp -rf $datadir/* $refdir

echo "SUCCESS: Open data updated !"

echo "Restarting web server..."
systemctl restart blablactesweb.service
```

# SSL

## Génération certificat

Autorité de certification : let's encrypt  
Voir doc Let's encrypt

## Utilisation du certificat

Utilisation de la notion de Secret asp.net <https://docs.microsoft.com/en-us/dotnet/architecture/microservices/secure-net-microservices-web-applications/developer-app-secrets-storage>

Le fichier de secret est de la forme:
```json
{
  "Kestrel": {
    "Certificates": {
      "Default": {
        "Path": "path/to/cert.pem",
        "KeyPath": "path/to/key.pem",
        "Password": "pass protecting the key"        
      }
    }
  }
}
```

Stockage du secret:
- sous windows : `C:\Users\<username>\AppData\Roaming\Microsoft\UserSecrets\<secret-id>\secrets.json`
- sous linux : `~/.microsoft/usersecrets/<user_secrets_id>/secrets.json`

*notes* : pour le moment, pas réussi à utiliser le secret sur le serveur (.net le trouve pas le fichier)