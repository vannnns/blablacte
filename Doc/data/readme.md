# Sources

Les données viennent de l'open data Français.
- liste des scrutins : <https://data.assemblee-nationale.fr/travaux-parlementaires/votes>
- liste des acteurs (députés, organes, mandats) : <https://data.assemblee-nationale.fr/acteurs/deputes-en-exercice>

Documentation open data assemblée <https://www.assemblee-nationale.fr/opendata/index_pub.html>

# Formats

Les schémas ne sont pas fournis par data.assemblee-nationale.fr ni par <https://schema.data.gouv.fr/schemas.html>
Il faudra les définir nous même.

## Liste des scrutins

Zip contenant un json par scrutin de la 15ème législature (juin 2017)

Le format des fichiers json est décrit ici: [scrutins-schema.json](scrutins-schema.json)

# Association scrution > dossier législatif

Il n'y a pas de lien direct qui permet de lier un scrutin à son dossier législatif.
Le lien trouvé est le suivant:
```
Scrutin [sessionRef] -> Réunion [ODJ.pointsODJ.dossiersLegislatifsRefs.dossierRef] -> Dossier
```

Comme il peut y avoir plusieurs points à l'ODJ (ordre du jour) d'une réunion, et plusieurs dossiers législatifs par point, le choix a été fait de, lorsqu'il y a plusieurs points dans une même réunion:
- prendre le point dont l'objet se rapproche le plus du titre du scrutin (par fuzzy matching)

