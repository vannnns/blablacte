open System
open Domain.AssemblyTypes
open Domain.Search
open OpenDataParsing.Parser
open System.IO
open FSharp.Data
open Domain.NLP
open System.Collections.Generic
open Microsoft.ML
open Microsoft.ML.Data
open Domain.NLP2

[<EntryPoint>]
let main argv =
    
    //let res = matchScore "Lecture définitive de la proposition de loi visant à combattre le harcèlement scolaire et le cyberharcèlement" "l'ensemble de la proposition de loi visant à démocratiser le sport en France (lecture définitive)."
    //let res2 = matchScore "Lecture définitive de la proposition de loi visant à démocratiser le sport, à améliorer la gouvernance des fédérations sportives et à sécuriser les conditions d’exercice du sport professionnel" "l'ensemble de la proposition de loi visant à démocratiser le sport en France (lecture définitive)."

    //let openDataDirectory = @"C:\Users\Vans\Documents\dev\data-blablacte"
    //let organs = loadDirectory @"C:\Users\Vans\Downloads\AMO10_deputes_actifs_mandats_actifs_organes_XV.json\json\organe" loadOrgan (fun party -> party.Uid)
    //let actors = loadDirectory @"C:\Users\Vans\Documents\dev\data-blablacte\acteur" loadActor (fun actor -> actor.Uid)

    // let party = loadOrgan @"C:\Users\Vans\Downloads\AMO10_deputes_actifs_mandats_actifs_organes_XV.json\json\organe\PO761294.json"
    // let voting = loadVoting actors organs @"C:\Users\Vans\Downloads\Scrutins_XV.json\json\VTANR5L15V1208.json"
    // let actor = loadActor @"C:\Users\Vans\Downloads\AMO10_deputes_actifs_mandats_actifs_organes_XV.json\json\acteur\PA508.json"
    // let legislativeFile = loadLegislativeFile @"C:\Users\Vans\Downloads\Dossiers_Legislatifs_XV.json\json\dossierParlementaire\DLR5L15N40346.json"

    // let legislativeFiles = loadDirectory @"C:\Users\Vans\Downloads\Dossiers_Legislatifs_XV.json\json\dossierParlementaire\" loadLegislativeFile (fun file -> file.Uid)

    // let legislativeFiles = loadDirectory (Path.Combine(openDataDirectory, "dossierParlementaire", "json", "dossierParlementaire")) loadLegislativeFile (fun file -> file.Uid)


//    let meetings = loadDirectory @"C:\Users\Vans\Downloads\Agenda_XV.json\json\reunion" loadMeeting (fun meeting -> meeting.Uid)

    //let r = meetings.Values |> Seq.sumBy (fun m -> if m.LegislativeFileRefs.Length > 1 then 1 else 0) 

    //let r2 = meetings.Values |> Seq.sumBy (fun m -> if m.LegislativeFileRefs.Length < 1 then 1 else 0) 
    // let inversed = inverseAll legislativeFiles
   // let votings = loadDirectory @"C:\Users\Vans\Downloads\Scrutins_XV.json\json" (loadVoting actors organs meetings) (fun voting -> voting.Number)
    
    // let votingsWithSeveralLegislativeFiles = 
    //     votings.Values 
    //     |> Seq.filter (fun v -> 
    //         match v.Seance with
    //         | Some ({Points = pts}) when pts.Length > 1 -> true
    //         | _ -> false)
    //     |> Seq.toList
    
    // let lastVoting = lastVoting votings
    // let forVotesOrgans = getOrgans organs actors (List.head lastVoting.ForVotes)

    // let politicalParties = List.filter (fun p -> p.Type = "PARPOL") forVotesOrgans

   // let titles = Seq.map (fun v -> v.Title) votings.Values |> Seq.toList
    //let preProcessedTitles = preProcessTitles votings


    
    //while true do
    //    Console.Write "Enter scrutin search : "
    //    let q = Console.ReadLine() 
    //    let responses = getTopNScrutins 10 q votings preProcessedTitles
    //    List.iter (fun (v : Voting) -> printfn "%s" v.Title) responses        
            
    //0

    buildBagOfWords2