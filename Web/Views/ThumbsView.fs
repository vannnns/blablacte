namespace Views

module ThumbsView =
    open Giraffe.ViewEngine
    open Domain.AssemblyTypes

    let private renderSynthesis (title : string) (n : int) (class' : string) (renderTitles : bool) (isLeft : bool) : XmlNode =
        
        let posClass = if (isLeft) then " has-tooltip-left" else ""
        let attributes = 
            if renderTitles then 
                [_class ("has-tooltip-arrow is-tooltip-larger" + posClass) ; attr "data-tooltip" title]
            else
                []
        span attributes [
            string n |> encodedText 
            encodedText " "
            i [ _class class'] []
            encodedText " "
        ]

    let renderThumbs (forVotes : int) (againstVotes : int) (abstentionVotes : int) (nonVotes : int) (renderTitles : bool) : XmlNode list =
        [
            renderSynthesis "Votes pour" forVotes "fas fa-thumbs-up colorFor" renderTitles false
            encodedText " / "
            renderSynthesis "Votes contre" againstVotes "fas fa-thumbs-down colorAgainst" renderTitles false
            encodedText " / "
            renderSynthesis "Abstentions" abstentionVotes "fas fa-thumbs-up fa-rotate-270 colorAbstention" renderTitles false
            encodedText " / "
            renderSynthesis "Non votants (président·e·s de l'assemblée, de séance, etc.)" nonVotes "fas fa-times colorNon" renderTitles true
        ]