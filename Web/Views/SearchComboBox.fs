namespace Views

module SearchComboBox =
    open System.Web
    open Giraffe.ViewEngine
    open Domain.AssemblyTypes

    let renderSearchResult (voting : Voting) : XmlNode list =
        [
            a [ _class "dropdown-item link-combo"; _tabindex "0" ; _href (sprintf "/votings/%i" voting.Number)] [
                (string voting.Number + " - " + voting.Title) |> encodedText 
            ]
        ]

    let renderSearchComboBox (votings : Voting seq) : XmlNode =

        let last10 = Seq.rev votings |> Seq.take 10

        div [_id "searchDropDown"; _class "dropdown is-large"] [
            div [_class "dropdown-trigger"] [
                div [ _class "field"] [
                    p [_class "control is-expanded has-icons-right"] [
                        input [ _class "input test"; _type "search"; _placeholder "Rechercher un scrutins..."]
                        span [ _class "icon is-small is-right"] [
                            i [ _class "fas fa-search"] []
                        ]                        
                    ]
                ]
            ]
            div [ _id "dropdown-menu"; _class "dropdown-menu"; attr "role" "menu"] [
                div [ _class "dropdown-content has-background-primary-light"] [
                    yield! Seq.collect renderSearchResult last10
                ]
            ]
        ]