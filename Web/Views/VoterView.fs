namespace Views

module VoterView =
    open Giraffe.ViewEngine
    open Domain.AssemblyTypes

    let private renderThumb (result : VotingResult) : XmlNode =
        let styleFontSize = _style "font-size: 1em;"

        match result with
        | Non -> i [styleFontSize; _class "fas fa-times colorNon"] []
        | For -> i [styleFontSize; _class "fas fa-thumbs-up colorFor" ] []
        | Against -> i [styleFontSize; _class "fas fa-thumbs-down colorAgainst" ] []
        | Abstention -> i [styleFontSize; _class "fas fa-thumbs-up fa-rotate-270 colorAbstention"] []
        

    let renderVoter (voter : Voter) (result : VotingResult) : XmlNode * XmlNode = 
        (
            td [_style "vertical-align: middle"] [
                a [ _href ("https://www2.assemblee-nationale.fr/deputes/fiche/OMC_" + voter.Actor.Uid) ] [
                    voter.Actor.FirstName + " " + voter.Actor.LastName
                    |> encodedText
                ] 
            ],
            td [_style "vertical-align: middle" ] [
               renderThumb result
            ]
        ) 

