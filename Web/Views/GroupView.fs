namespace Views

module GroupView =
    open Giraffe.ViewEngine
    open Domain.AssemblyTypes
    open Views.VoterView
    open Views.ThumbsView

    let col = tag "col"

    let makePairs (elems : 'a list) : ('a * 'a option) list =
        let rec makePairsImp (pairs : ('a * 'a option) list) (xs : 'a list) : ('a * 'a option) list =
            match xs with
            | [] -> pairs
            | x :: [] -> (x, None) :: pairs
            | x :: y :: xs -> makePairsImp ((x, Some y) :: pairs) xs
        in
            makePairsImp [] elems
            |> List.rev

    let private renderRow2 (result : VotingResult) (v1 : Voter, v2 : Voter option) : XmlNode =
        let (td1, td2) = renderVoter v1 result
        match v2 with
        | Some v -> 
            let (td3, td4) = renderVoter v result 
            tr [] [td1; td2; td3; td4]
        | None -> 
            tr [] [td1; td2; td [] []; td [] []] 

    let private renderVoters (result : VotingResult) (voters : Voter list) : XmlNode list =
        List.map (renderRow2 result) (makePairs voters)

    let private renderGroupTitle (g : Group) : XmlNode =

        let renderSynthesis (n : int) (class' : string) : XmlNode list =
            [
                string n |> encodedText 
                encodedText " "
                i [ _class class'] []
                encodedText " "
            ]

        div [_class "columns is-gapless is-multiline group-title"] [
            div [_class "column is-full is-size-4"] [
                encodedText g.Organ.Name
                span [_class "is-size-7 nowrap"] [ 
                    encodedText " (" 
                    encodedText (string g.MembersCount)
                    encodedText " " 
                    i [_class "fas fa-users"] []
                    encodedText ")"
                ]
            ] 
            div [_class "column is-full is-size-7"] (renderThumbs g.ForVotes.Length g.AgainstVotes.Length g.AbstentionVotes.Length g.NonVotes.Length false)
        ]
        
    let private renderHeader (g : Group) : XmlNode =
        div [_class "columns is-gapless is-mobile is-clickable group-header"] [
            div [_class "column is-11"] [
                renderGroupTitle g
            ]
            div [_class "column is-1 is-flex is-justify-content-center is-align-items-center"] [
                button [_class "card-header-icon"; _title "Détails..."] [
                    span [_class "icon"] [
                        i [_class "fas fa-angle-down"] []
                    ]
                ]
            ]
        ]
        
    let renderGroupTale (g : Group) : XmlNode =
        div [_class "column is-4 is-4-fullhd is-6-desktop is-12-tablet is-12-mobile"] [ 
            div [_class "card mb-2"] [
                renderHeader g
                div [_class "content"] [
                    table [_class "table is-narrow voting-table"] [
                        col [_style "width:49%"] []
                        col [_style "width:1%"] []
                        col [_style "width:49%"] []
                        col [_style "width:1%"] []
                        thead [] [
                            tr [] [
                                th [_colspan "4"] [                                    
                                ]
                            ]
                        ]
                        tbody [] [
                            yield! renderVoters For g.ForVotes
                            yield! renderVoters Against g.AgainstVotes
                            yield! renderVoters Abstention g.AbstentionVotes
                            yield! renderVoters Non g.NonVotes
                        ]
                    ]
                ]
            ]
       ]