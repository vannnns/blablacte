namespace Views

module LandingPage =
    open System.Web
    open Giraffe.ViewEngine
    open Domain.AssemblyTypes
    open Views.GroupView
    open Views.ThumbsView
    open System.Globalization
    open Views.SearchComboBox
    open Domain.Search

    let private FRfrCultureInfo = new CultureInfo("fr-FR")

    let private renderSynthesis (title: string) (n: int) (class': string) : XmlNode =
        span [ _title title ] [
            string n |> encodedText
            encodedText " "
            i [ _class class' ] []
            encodedText " "
        ]

    let escapeQuote (s: string) : string = s.Replace(@"""", @"\""")

    let renderTitlesScript (votings: Votings) : XmlNode =
        script [] [
            rawText @"var titles = ["""
            String.concat @""",""" (Seq.map (fun v -> escapeQuote v.Title) votings.Values)
            |> rawText
            rawText @"""];"
        ]

    let private toTitleCase (s : string) : string = 
        let firstLetter = 
            s[0]
            |> System.Char.ToUpper
            |> string
        firstLetter + s[1..]
        

    let renderLandingPage (votings: Votings) (votingID: int) =
        let voting =
            match votings.TryFind votingID with
            | Some v -> v
            | None -> failwith "erreor pas content"

        let titleCase = toTitleCase voting.Title

        html [ _lang "fr" ] [
            head [] [
                meta [ _charset "UTF-8" ]
                title [] [ encodedText "Blablactes" ]
                meta [ _name "viewport"
                       _content "width=device-width, initial-scale=1.0" ]
                meta [ _name "description"
                       _content (sprintf "L'ensemble des scrutins votés à l'assemblée nationale.\n Le %s : %s" 
                        (voting.Date.ToString("D", FRfrCultureInfo.DateTimeFormat)) titleCase) ]
                link [ _rel "icon"
                       _type "image/png"
                       _href "/img/miniblabla.png" ]
                link [ _rel "stylesheet" 
                       _type "text/css"
                       _href "/css/main.css" ]
                link [ _rel "stylesheet"
                       _type "text/css"
                       _href "https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css" ]
                link [ _rel "stylesheet"
                       _type "text/css"
                       _href "https://bulma-tooltip.netlify.app//css/bulma-tooltip.min.css" ]
                link [ _rel "stylesheet"
                       _type "text/css"
                       _href "https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                       _integrity "sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                       _crossorigin "anonymous" ]
                //renderTitlesScript votings
                script [ _async
                         _src "https://unpkg.com/mithril/mithril.js" ] []
                script [ _async; 
                         _src "/js/drop.js" ] []
            ]
            body [ _class "bg" ] [
                section [ _class "hero is-primary is-assembly-blue" ] [
                    a [ _class "ribbon" 
                        _href "https://gitlab.com/vannnns/blablacte"] [ "Gitlab" |> rawText]
                    div [ _class "hero-body" ] [
                        p [ _class "title" ] [
                            a [_href "/"] [encodedText "Blablactes "]
                            span [ _class "is-size-7" ] [
                                encodedText "(du blabla, et les actes ?)"
                            ]
                        ]
                        p [ _class "subtitle" ] [
                            encodedText "L'ensemble des scrutins votés à l'assemblée nationale."

                            renderSearchComboBox votings.Values
                        ]
                    ]
                ]
                div [ _class "box has-background-grey-lighter main-box" ] [
                    p [ _class "title has-text-dark has-text-centered" ] [
                        match getBestPoint voting with
                        //takes the first LegislativeFileRef from the first point for now
                        | Some { LegislativeFileRef = ref :: _ } -> a [_href ("https://www.assemblee-nationale.fr/dyn/16/dossiers/" + ref)] [encodedText titleCase]
                        | _ -> span [] [encodedText titleCase]
                    ]
                    p [ _class "title has-text-dark has-text-centered" ]
                        (renderThumbs
                            voting.ForVotes.Length
                            voting.AgainstVotes.Length
                            voting.AbstentionVotes.Length
                            voting.NonVotes.Length true)
                    p [ _class "subtitle"] [
                        rawText "Le "
                        voting.Date.ToString("D", FRfrCultureInfo.DateTimeFormat)
                        |> encodedText
                    ]
                    div [ _class "columns is-multiline is-0-mobile" ] [                       
                        yield! List.map renderGroupTale voting.Groups
                    ]
                ]
            ]
        ]