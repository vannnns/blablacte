var groupHeaders = document.getElementsByClassName('group-header');

for (var i = 0; i < groupHeaders.length; i++) {
    var header = groupHeaders[i];

    var onClickhandler = function() {   
        var content = this.parentNode.getElementsByClassName('content')[0];
        content.classList.toggle('is-hidden');
    };

    header.addEventListener('click', onClickhandler.bind(header));
}

var searchDropDown = document.getElementById('searchDropDown');
var focusInCombo = false;

searchDropDown.addEventListener('focusin', function(event) {                     
    event.stopPropagation();     
    searchDropDown.classList.add('is-active');
    focusInCombo = true;
});

searchDropDown.addEventListener('focusout', function(event) {                     
    event.stopPropagation();     
    focusInCombo = false;
    //trick to let the event focusin to happen if it should happen before trying to hide the menu
    setTimeout(function() {if (!focusInCombo) searchDropDown.classList.remove('is-active');});
});

searchDropDown.addEventListener('change', function(event) {       
    
    var renderDropDownContent = function(jsonTitles) {
    
        var renderTitleResult = function(titleResult) {
            return [
                m('a', {class: 'dropdown-item link-combo', tabindex : '0', href: '/votings/' + titleResult.votingNumber}, titleResult.votingNumber + " - " + titleResult.title),
            ];
        };
    
        var titles;
        if (jsonTitles.length > 0) {
            titles = jsonTitles.flatMap(renderTitleResult);
        }
        else {
            titles = [
                m('div', {class: 'dropdown-item'},
                    m('p', {}, "Aucun scrutin trouvé.")
                )            
            ];
        }
    
        return m('div', {class: 'dropdown-content has-background-primary-light'}, 
            titles
        );    
    }    

    var dropInput = event.target;
    var id = "dropdown-menu";
    var divParent = document.getElementById(id);

    m.request({
        method: "GET",
        url: "/api/v1/searchTitles",
        params: { q: dropInput.value }
    })
    .then(function(titles) {
        var newDropDownContent = renderDropDownContent(titles);
        m.render(divParent, newDropDownContent);
    });
});


// var spanTitle = document.getElementById('votingTitle');
// spanTitle.addEventListener('click', function(event) {
//     var ref = spanTitle.dataset.seanceref;
//     m.request({
//         method: "GET",
//         url: "https://www.assemblee-nationale.fr/dyn/opendata/" + ref + ".json",
//     })
//     .then(function(res) {
//         var fileRef = res.ODJ.pointsODJ.pointODJ.dossiersLegislatifsRefs.dossierRef;
//         var urlLegislativeFile = "https://www.assemblee-nationale.fr/dyn/15/dossiers/" + fileRef;
//         window.location.href = urlLegislativeFile;
//     });
// });