module Web.App

open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open OpenDataParsing.Parser
open Argu
open Domain.AssemblyTypes
open Domain
open Domain.Search
open Views.LandingPage
open Api.Api

type CliArguments =
    | [<Mandatory;Unique>] OpenDataDirectory of path:string
    
    interface IArgParserTemplate with
        member s.Usage = 
            match s with
            | OpenDataDirectory _ -> "specify directory where opendata JSON files are stored. Expected directory content : \n\
                                        - .\\organe : organs json files\n\
                                        - .\\acteur : actors json files\n\
                                        - .\\scrutin\\json : votings json files\n
                                        - .\\reunion\\json\\reunion : meetings json files\n"

// ---------------------------------
// Web app
// ---------------------------------

let indexHandler (votings : Votings) (votingID : int) =        
    let view = renderLandingPage votings votingID
    htmlView view

let private handleMainPage (votings : Votings) (votingNumber : int) =
    match votings.TryFind votingNumber with
        | Some v -> indexHandler votings votingNumber
        | None -> setStatusCode 404 >=> text (sprintf "Voting not found: %i" votingNumber)

let webApp (votings : Votings) (preProcTitles : PreprocessedTitle list) =
    choose [
        subRoute "/api" (apiHandler votings preProcTitles)
        GET >=>
            choose [
                route "/" >=> (handleMainPage votings (lastVotingID votings))
                routef "/votings/%i" (handleMainPage votings)
            ]
        setStatusCode 404 >=> text "Page not found" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins(
            "http://localhost:8040")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let configureApp (votings : Votings) (preProcTitles : PreprocessedTitle list) (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (
        match env.IsDevelopment() with
        | true  ->
            app.UseDeveloperExceptionPage()
        | false ->
            app.UseGiraffeErrorHandler(errorHandler)
    )
        .UseResponseCompression()
        //.UseHttpsRedirection() 
        .UseStaticFiles()
        .UseGiraffe(webApp votings preProcTitles)

let configureServices (services : IServiceCollection) =
    services.AddResponseCompression(
        fun options -> 
            options.EnableForHttps <- true) 
    |> ignore
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main args =

    let argsParser = ArgumentParser.Create<CliArguments>();
    let arguments = argsParser.Parse args

    let openDataDirectory = arguments.GetResult OpenDataDirectory

    let votings = loadVotings openDataDirectory
    let preProcTitles = preProcessTitles votings
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "WebRoot")
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .UseWebRoot(webRoot)
                    .Configure(Action<IApplicationBuilder> (configureApp votings preProcTitles))
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()
    0