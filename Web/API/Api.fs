namespace Api

open System
open Microsoft.AspNetCore.Http
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open OpenDataParsing.Parser
open Argu
open Domain.AssemblyTypes
open Domain
open Domain.Search

module Api = 

    type SearchTitlesAPIModel =
        {
            VotingNumber : int
            Title : string
        }

    let private toApiModelSearchTitles (votings : Voting list) : SearchTitlesAPIModel list =
        votings 
        |> List.map (fun v -> {VotingNumber = v.Number; Title = v.Title})

    let searchTitlesHandler (votings : Votings) (preProcTitles : PreprocessedTitle list) : HttpHandler =
        fun (next : HttpFunc) (ctx : HttpContext) ->
            match ctx.GetQueryStringValue "q" with
            | Error msg ->
                RequestErrors.badRequest (text msg) next ctx
            | Ok q ->                
                let res = 
                    if q.Length > 400 then //prevent attacks with huge query
                        []
                    else
                        getTopNScrutins 10 q votings preProcTitles
                        |> toApiModelSearchTitles
                json res next ctx

    let apiHandler (votings : Votings) (preProcTitles : PreprocessedTitle list) : HttpHandler = 
        subRoute "/v1"
            (choose [
                route "/searchTitles" >=> searchTitlesHandler votings preProcTitles
            ])