const path = require('path');
module.exports = {
   mode: "development",
   entry: "./src/lib.ts",
   output: {
       filename: "bundle.js",
       path: path.resolve(__dirname, '../WebRoot/js'),
       library: {
        name: 'blablalib',
        type: 'umd',
      },
   },
   resolve: {
       extensions: [".webpack.js", ".web.js", ".ts", ".js"]
   },
   module: {
       rules: [{ test: /\.ts$/, loader: "ts-loader" }]
   }
}