import * as fuzzy from 'fast-fuzzy'

export function sayHello() {
    var list = ["abc", "abcd", "acd", "zerzer", "abc"];
    return fuzzy.search("abc", list);
}

export var fuzzys = fuzzy.search