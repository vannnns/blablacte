﻿module AssemblyDataValidator

open OpenDataParsing.Parser
open System.IO
open Argu
open Domain.AssemblyTypes
open Domain
open Domain.Search

type CliArguments =
    | [<Mandatory;Unique>] OpenDataDirectory of path:string

    interface IArgParserTemplate with
        member s.Usage = 
            match s with
            | OpenDataDirectory _ -> "specify directory where opendata JSON files are stored. Expected directory content : \n\
                                        - .\\organe : organs json files\n\
                                        - .\\acteur : actors json files\n\
                                        - .\\scrutin\\json : votings json files\n
                                        - .\\reunion\\json\\reunion : meetings json files\n"

type ValidityResult =
    | OK
    | ParsingError of string


let validate (openDataDirectory : string) : ValidityResult =
    printfn "Loading organes..."
    try
        let organs = loadDirectory (Path.Combine(openDataDirectory, "organe")) loadOrgan (fun party -> party.Uid)
        printfn "Loading organes OK !"
        printfn "Loading acteurs..."
        try
            let actors = loadDirectory (Path.Combine(openDataDirectory,"acteur")) loadActor (fun actor -> actor.Uid)
            printfn "Loading acteurs OK !"
            printfn "Loading meetings..."
            try
                let meetings = loadDirectory (Path.Combine(openDataDirectory, "reunion", "json", "reunion"))  loadMeeting (fun meeting -> meeting.Uid)
                printfn "Loading meetings OK !"
                try
                    printfn "Loading Scrutins..."
                    let votings = loadDirectory (Path.Combine(openDataDirectory, "scrutin", "json")) (loadVoting actors organs meetings) (fun voting -> voting.Number)
                    printfn "Loading scrutins OK !"
                    OK
                with
                | ex -> 
                    printfn "Error while loading scrutins !"
                    ParsingError ex.Message 
            with
            | ex -> 
                printfn "Error while loading meetings !"
                ParsingError ex.Message
        with
        | ex -> 
            printfn "Error while loading acteurs !"
            ParsingError ex.Message
    with
    | ex -> 
        printfn "Error while loading organes !"
        ParsingError ex.Message

    
[<EntryPoint>]
let main args =

    let argsParser = ArgumentParser.Create<CliArguments>();
    let arguments = argsParser.Parse args

    let openDataDirectory = arguments.GetResult OpenDataDirectory

    match validate openDataDirectory with
    | OK -> printfn "Assembly open data are valid"; 0
    | ParsingError error -> printfn "Assembly open data are invalid: '%s'" error; 1
