#!/bin/bash

#15 ieme mandature
#urlScrutins="https://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.json.zip"
#urlOrganesActeurs="https://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises_XV.json.zip"
#urlReunions="https://data.assemblee-nationale.fr/static/openData/repository/15/vp/reunions/Agenda_XV.json.zip"

#16 ieme mandature
urlScrutins="https://data.assemblee-nationale.fr/static/openData/repository/16/loi/scrutins/Scrutins.json.zip"
urlOrganesActeurs="https://data.assemblee-nationale.fr/static/openData/repository/16/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises.json.zip"
urlReunions="https://data.assemblee-nationale.fr/static/openData/repository/16/vp/reunions/Agenda.json.zip"

srvdir="/srv/blablactes"
refdir="$srvdir/data"
tmpdir="/tmp/blablactes/"
datadir="/tmp/blablactes/data"
tmpScrutins="scrutins.zip"
tmpAO="ao.zip"
tmpReu="reu.zip"
scrutinsZip="$tmpdir$tmpScrutins"
aoZip="$tmpdir$tmpAO"
reuZip="$tmpdir$tmpReu"

mkdir -p $datadir

echo "Downloading scrutins..."
curl -o $scrutinsZip $urlScrutins
echo "done"

echo "Downloading organesActeurs..."
curl -o $aoZip $urlOrganesActeurs
echo "done"

echo "Downloading réunions..."
curl -o $reuZip $urlReunions
echo "done"

echo "Unziping $scrutinsZip to $datadir/scrutin"
unzip -qo $scrutinsZip -d "$datadir/scrutin"

echo "Unziping $aoZip to $datadir"
unzip -qo $aoZip -d $datadir

echo "Unziping $reuZip to $datadir/reunion"
unzip -qo $reuZip -d "$datadir/reunion"

echo "Unzipping OK"

echo "Check data consistency..."
dotnet $srvdir/batch/assemblydatavalidator/AssemblyDataValidator.dll --opendatadirectory $datadir

if [ $? -eq 0 ]
then
  echo "Data consistency OK."
else
  echo "ERROR : Data consistency KO"
  exit 1
fi

echo "Copying data to $refdir ..."
cp -rf $datadir/* $refdir

echo "SUCCESS: Open data updated !"

echo "Restarting web server..."
systemctl restart blablactesweb.service