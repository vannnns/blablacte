﻿namespace Domain

open System

module AssemblyTypes =
    type Point =
      { Objet : string
        LegislativeFileRef : string list} 

    type Meeting =
      { Uid : string
        Points : Point list }        

    type LegislativeFile = 
      { Uid : string
        Title : string 
        VotingsUid : string list } 

    type VotingResult = 
      | Non
      | For
      | Against
      | Abstention

    type Mandate = 
        { Uid : string 
          OrgansUid : string list
          StartingDate : DateTime 
          EndDate : DateTime Option }

    type Organ =
        { Uid : string
          Type : string
          Name : string
          ShortName : string }

    type Organs = Map<string, Organ>
    
    type Actor =
        { Uid : string
          FirstName : string
          LastName : string 
          Mandates : Mandate list }

    type Actors = Map<string, Actor>

    type Voter =
        { Actor: Actor 
          MandateRef: string }

    type VotingSynthesis =
        { VotersNumber : int 
          VotesCast : int 
          RequiredCastNumber : int }

    type Group =
        { Organ : Organ 
          MembersCount : int
          NonVotes: Voter list 
          ForVotes: Voter list 
          AgainstVotes: Voter list
          AbstentionVotes: Voter list  }

    type Voting =
        { Uid : string
          Number : int
          Title : string
          Date : DateTime
          Synthesis : VotingSynthesis
          NonVotes: Voter list 
          ForVotes: Voter list 
          AgainstVotes: Voter list
          AbstentionVotes: Voter list 
          Groups : Group list
          Seance : Meeting option }    

    type Votings = Map<int, Voting>