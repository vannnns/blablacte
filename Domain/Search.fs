﻿namespace Domain

open System
open Domain.AssemblyTypes
open NLP

module Search = 
    
    type PreprocessedTitle = 
        { VotingNumber : int
          SentenceWords : string[] }

    let lastVotingID (votings : Votings) : int = 
        Seq.max votings.Keys

    let lastVoting (votings : Votings) : Voting =
        votings[lastVotingID votings]

    let getOrgans (parties : Map<string, Organ>) (actors : Actors) (voter : Voter) : Organ list =
        let actor = voter.Actor
        let mandates = actor.Mandates
        let partiesUid = List.collect (fun m -> m.OrgansUid) mandates
        List.map (fun (uid : string) -> parties[uid]) partiesUid
  
    let preProcessTitles (votings : Votings) : PreprocessedTitle list =
        Seq.map (fun (v:Voting) -> {VotingNumber = v.Number; SentenceWords = preProcess v.Title}) votings.Values |> Seq.toList

    let toVotingFileUids (legislativeFiles : Map<string, LegislativeFile>) : Map<string, string> =
        Map.fold (
            fun (state : ResizeArray<string * string>) k v ->                 
                for ref in v.VotingsUid do
                    state.Add (ref, k)
                state
            ) 
            (new ResizeArray<string * string>()) legislativeFiles
        |> Map.ofSeq

    let getTopNScrutins (top : int) (query : string) (votings : Votings) (preProcTitles : PreprocessedTitle list) : Voting list =
        let preQuery = preProcess query
        let responses = 
            List.map (fun (pt) -> (sentenceScore preQuery pt.SentenceWords, pt.VotingNumber)) preProcTitles
            |> List.sortBy (fst >> (-) 0) 
        let topN = 
            List.take top responses
            |> List.filter (fun (score, num) -> score > 0)

        List.map (fun (score, num) -> Map.find num votings) topN

    let private evaluatePoint (matchingTitle : string) (point : Point) : int =
        -matchScore point.Objet matchingTitle 

    let getBestPoint (voting : Voting) : Point option =
        let votingTitle = voting.Title
        match voting.Seance with
        | Some s -> 
            let pointsWithFile = List.filter (fun p -> p.LegislativeFileRef.Length > 0) s.Points
            let sorted = List.sortBy (evaluatePoint votingTitle) pointsWithFile
            sorted
            |> List.head
            |> Some
        | _ -> None