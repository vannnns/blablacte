# blablacte

<https://blablactes.fr>

Du blabla et des actes.  
Visualisation claire et instantanée des votes de l'assemblée nationale Française. Ils disent du blabla, quid des actes (votes) ?

# Architecture

- AssemblyDataValidator : download et vérifie l'intégrité des données de l'open data de l'assemblée nationale
- Console : programme de tests manuels
- Domain : contient les types représentant un ensemble de scrutins. Contient également les fonctions de recherche sur ce modèle
- Web : application web asp.net 6 basée sur le framework Giraffe <https://github.com/giraffe-fsharp/Giraffe> et affichant les résutlats des scrutins de façon "claire et instantanée" :)
- ts : bibliothèque pour les fonctions utilisée par les pages

# Documentation

<https://gitlab.com/vannnns/blablacte/-/blob/main/Doc/>

# Build project

`dotnet publish -r linux-x64 --self-contained false -c Release`

