﻿namespace OpenDataParsing

open FSharp.Data.JsonExtensions
open FSharp.Data
open Domain.AssemblyTypes
open System.IO
open System
open System.Text.RegularExpressions

module Parser =    

    let private parseDate (date : string) : DateTime =
        let mutable d = DateTime.MinValue       
        if DateTime.TryParse(date, &d) = true then
            d
        else        
            failwith (sprintf "invalid datetime '%s'" date)

    // Actor parsing

    let parseMandate (mandat : JsonValue) : Mandate =
        let endDate = 
            let dateFin = mandat?dateFin
            match dateFin with
            | JsonValue.Null -> None
            | JsonValue.String s -> Some ( s |> parseDate)
            // | JsonValue.Record _ -> Some (dateFin.AsString() |> parseDate)
            | _ -> failwith "invalid json structure, expected dateFin element"

        let partiesUid = 
            let organeRef = mandat?organes?organeRef
            match organeRef with
            | JsonValue.String s -> [s]
            | JsonValue.Array ors -> Array.map (fun (o : JsonValue) -> o.AsString()) ors |> Array.toList
            | _ -> failwith "invalid json structure, expected string or [string] for organeRef elements"

        { Uid = mandat?uid.AsString()
          OrgansUid = partiesUid
          StartingDate = mandat?dateDebut.AsString() |> parseDate
          EndDate = endDate }

    let parseActor (content : JsonValue) : Actor =
        let acteur = content?acteur       
        let mandatsJSON = acteur?mandats?mandat
        let mandates = 
            match mandatsJSON with
            | JsonValue.Array ms -> Array.fold (fun mandats m -> parseMandate m :: mandats) [] ms
            | _ -> failwith "invalid json structure, expected Array as mandat elements"
        { Uid = acteur?uid.GetProperty("#text").AsString()
          FirstName = acteur?etatCivil?ident?prenom.AsString()
          LastName = acteur?etatCivil?ident?nom.AsString() 
          Mandates = mandates }


    // Voting pasring

  

    let private parseSynthesis (syntheseVote : JsonValue) : VotingSynthesis =
        { VotersNumber = syntheseVote?nombreVotants.AsInteger()
          VotesCast = syntheseVote?suffragesExprimes.AsInteger()
          RequiredCastNumber = syntheseVote?nbrSuffragesRequis.AsInteger() }

    let private createUnknownActor (acteurRef : string) : Actor =
          { Uid = acteurRef            
            FirstName = sprintf "Unknown : %s" acteurRef
            LastName = "?" 
            Mandates = [] }

    let private parseVoter (actors : Actors) (votant : JsonValue) : Voter =
        let acteurRef = votant?acteurRef.AsString()
        let actor =
            match actors.TryFind acteurRef with 
            | Some a -> a
            | None -> createUnknownActor acteurRef
        { Actor = actor 
          MandateRef = votant?mandatRef.AsString() }

    let private extractKindOfVoters (actors : Actors) (groupJSON : JsonValue) (whichVoterGroup : JsonValue -> JsonValue) : Voter list =
        let whichVoters = whichVoterGroup(groupJSON?vote?decompteNominatif)
        if whichVoters = JsonValue.Null then
            []
        else    
            let votant = whichVoters?votant
            match votant with
            | JsonValue.Null -> []
            | JsonValue.Record _ -> [parseVoter actors votant]
            | JsonValue.Array nvs -> [for nv in nvs do parseVoter actors nv]
            | _ -> failwith "invalid json structure, expected Array as groupJSON?vote?decompteNominatif?nonVotants?votant elements"

    let private parseKindOfVoters (actors : Actors) (scrutin : JsonValue) (whichVoterGroup : JsonValue -> JsonValue) : Voter list =                
        let groupsJSON = scrutin?ventilationVotes?organe?groupes?groupe        
        match groupsJSON with
            | JsonValue.Array gs -> Array.fold (fun voters g -> voters @ (extractKindOfVoters actors g whichVoterGroup)) [] gs
            | _ -> failwith "invalid json structure, expected Array as group elements"

    let private parseOrgan (content : JsonValue) : Organ =
        let organe = content?organe
        { Uid = organe?uid.AsString()
          Type = organe?codeType.AsString()
          Name = organe?libelle.AsString() 
          ShortName = organe?libelleAbrev.AsString() }

    let private createUnknownOrgan (organeRef : string) : Organ =
        { Uid = organeRef
          Type = "Unknown"
          Name = sprintf "Unknown : %s" organeRef
          ShortName = "?" }

    let private safeGet (getter : JsonValue -> JsonValue) (node : JsonValue): JsonValue =
        try
            getter node
        with
            | _ -> JsonValue.Null

    let private parseGroup (actors : Actors) (organs : Organs) (group : JsonValue) : Domain.AssemblyTypes.Group =
        let organeRef = group?organeRef.AsString()
        let membersCount = group?nombreMembresGroupe.AsInteger()
        let organ =
            match organs.TryFind organeRef with 
            | Some o -> o
            | None -> createUnknownOrgan organeRef        
        let nonVoters = extractKindOfVoters actors group (
            fun decompteNominatif -> 
                try
                    decompteNominatif?nonVotants
                with
                    | _ -> JsonValue.Null
            )
        let forVoters = extractKindOfVoters actors group (safeGet (fun decompteNominatif -> decompteNominatif?pours))
        let againstVoters = extractKindOfVoters actors group (safeGet (fun decompteNominatif -> decompteNominatif?contres))
        let abstentionVoters = extractKindOfVoters actors group (safeGet (fun decompteNominatif -> decompteNominatif?abstentions))
        { Organ = organ 
          MembersCount = membersCount
          NonVotes = nonVoters
          ForVotes = forVoters 
          AgainstVotes = againstVoters
          AbstentionVotes = abstentionVoters }
        

    let private parseGroups (actors : Actors) (organs : Organs) (groupsJSON : JsonValue) : Domain.AssemblyTypes.Group list =         
        match groupsJSON with
            | JsonValue.Array gs -> Array.map (parseGroup actors organs) gs |> Array.toList
            | _ -> failwith "invalid json structure, expected Array as group elements"

   

    let parseVoting (actors : Actors) (organs : Organs) (meetings : Map<string, Meeting>) (content : JsonValue) : Voting =
        let scrutin = content?scrutin
        let number = scrutin?numero.AsInteger()
        let title = scrutin?titre.AsString()
        let date = parseDate(scrutin?dateScrutin.AsString())
        let uid = scrutin?uid.AsString()
        let synthesis = parseSynthesis scrutin?syntheseVote        
        let nonVoters = parseKindOfVoters actors scrutin (safeGet (fun decompteNominatif -> decompteNominatif?nonVotants))            
        let forVoters = parseKindOfVoters actors scrutin (safeGet (fun decompteNominatif -> decompteNominatif?pours))
        let againstVoters = parseKindOfVoters actors scrutin (safeGet (fun decompteNominatif -> decompteNominatif?contres))
        let abstentionVoters = parseKindOfVoters actors scrutin (safeGet (fun decompteNominatif -> decompteNominatif?abstentions))
        let groups = parseGroups actors organs scrutin?ventilationVotes?organe?groupes?groupe  
        let seanceRef = content?scrutin?seanceRef.AsString()
        { Uid = uid       
          Number = number
          Title = title
          Date = date
          Synthesis = synthesis
          NonVotes =  nonVoters
          ForVotes = forVoters
          AgainstVotes = againstVoters
          AbstentionVotes = abstentionVoters 
          Groups = groups 
          Seance = Map.tryFind seanceRef meetings }
    
    let private getFirstGroupValue (m : Match) : string =
        (List.tail [for g in m.Groups -> g.Value]).Head

    let parseLegislativeFile (content : string) : LegislativeFile =
        let contentHead = content.Substring(0, 600)
        let titleMatch = Regex.Match(contentHead, @"""titre"": ""([^""]+)""")
        let title =
            if titleMatch.Success then
                getFirstGroupValue titleMatch
            else
                "Title Not Found"

        let uidMatch = Regex.Match(contentHead, @"""uid"": ""([^""]+)""")
        let uid =
            if uidMatch.Success then
                getFirstGroupValue uidMatch
            else
                "Uid Not Found"

        let matches = Regex.Matches(content, @"""voteRef"": ""([^""]+)""")
        let refs : string list = 
            matches
            |> Seq.cast
            |> Seq.map getFirstGroupValue 
            |> Seq.toList

        { Uid = uid
          Title = title
          VotingsUid = refs }

    // let parseMeeting (content: string) : Meeting =
    //     let contentHead = content.Substring(0, 400)

    //     let uidMatch = Regex.Match(contentHead, @"""uid"": ""([^""]+)""")
    //     let uid =
    //         if uidMatch.Success then
    //             getFirstGroupValue uidMatch
    //         else
    //             "Uid Not Found" 

    //     let objetsMatchs = Regex.Matches(content, @"""objet"": ""([^""]+)""")
    //     let objets : string list = 
    //         objetsMatchs
    //         |> Seq.cast
    //         |> Seq.map getFirstGroupValue 
    //         |> Seq.toList 

    //     let matches = Regex.Matches(content, @"""dossierRef"": ""([^""]+)""")
    //     let refs : string list = 
    //         matches
    //         |> Seq.cast
    //         |> Seq.map getFirstGroupValue 
    //         |> Seq.toList 

    //     let points = 
    //         List.zip objets refs
    //         |> List.map (fun (o, r) -> {Objet = o; LegislativeFileRef = r})

    //     { Uid = uid
    //       Points =  points}

    let parseMeeting (content: JsonValue) : Meeting =

        let parsePoint (p : JsonValue) =
            let objet = p?objet.AsString()
            let dossierRefs = p?dossiersLegislatifsRefs
            let dossiersLegislatifsRef = 
                if dossierRefs <> JsonValue.Null then 
                    match dossierRefs?dossierRef with
                    | JsonValue.Null -> []
                    | JsonValue.String s -> [s]
                    | JsonValue.Array ds -> [for dossier in ds do dossier.AsString()]
                    | _ -> failwith "invalid json structure, expected Array or element as reunion?ODJ?pointsODJ?pointODJ?dossiersLegislatifsRefs?dossierRef elements"
                else 
                    []
            { Objet = objet
              LegislativeFileRef = dossiersLegislatifsRef }

        let reunion = content?reunion
        let uid = reunion?uid.AsString()
        let pts = 
            let ODJ = reunion.TryGetProperty("ODJ")            
            match ODJ with
            | Some v when v <> JsonValue.Null -> 
                let pointsODJ = v.TryGetProperty("pointsODJ")
                match pointsODJ with
                | Some v when v <> JsonValue.Null -> 
                    let points = v?pointODJ
                    match points with
                        | JsonValue.Null -> []
                        | JsonValue.Record _ -> [parsePoint points]
                        | JsonValue.Array pts -> [for pt in pts do parsePoint pt]
                        | _ -> failwith "invalid json structure, expected Array as reunion?ODJ?pointsODJ?pointODJ elements"
                | _ -> []                
            | _ -> []

        { Uid = uid
          Points = pts}

    // loaders

    let loadOrgan (path : string) : Organ =
        File.ReadAllText path
        |> JsonValue.Parse
        |> parseOrgan

    let loadVoting (actors : Actors) (organs : Organs) (meetings : Map<string, Meeting>) (path : string) : Voting =
        File.ReadAllText path
        |> JsonValue.Parse
        |> parseVoting actors organs meetings
 
    let loadActor (path : string) : Actor =
        File.ReadAllText path
        |> JsonValue.Parse
        |> parseActor 
    
    let loadLegislativeFile (path : string) : LegislativeFile =
        File.ReadAllText path        
        |> parseLegislativeFile 
 
    let loadMeeting (path : string) : Meeting =
        File.ReadAllText path        
        |> JsonValue.Parse
        |> parseMeeting

    let loadDirectory (dirPath : string) (fileLoader : string -> 'a) (getUID : 'a -> 'b): Map<'b, 'a> =
        Directory.GetFiles dirPath
        |> Array.fold 
            (fun m path -> 
                let loaded = fileLoader path
                Map.add (getUID loaded) (loaded) m)
            Map.empty            

    let loadVotings (openDataDirectory : string) : Votings =
        let organs = loadDirectory (Path.Combine(openDataDirectory, "organe")) loadOrgan (fun party -> party.Uid)
        let actors = loadDirectory (Path.Combine(openDataDirectory,"acteur")) loadActor (fun actor -> actor.Uid)
        let meetings = loadDirectory (Path.Combine(openDataDirectory, "reunion", "json", "reunion"))  loadMeeting (fun meeting -> meeting.Uid)
        loadDirectory (Path.Combine(openDataDirectory, "scrutin", "json")) (loadVoting actors organs meetings) (fun voting -> voting.Number)